#!/usr/bin/python3
# -*- coding: utf-8 -*-

#=========================================================================
#Copyright (c) June 2024

#Permission is hereby granted, free of charge, to any person obtaining a copy
#of this software and associated documentation files (the "Software"), to deal
#in the Software without restriction, including without limitation the rights
#to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
#copies of the Software, and to permit persons to whom the Software is
#furnished to do so, subject to the following conditions:

#The above copyright notice and this permission notice shall be included in all
#copies or substantial portions of the Software.

#THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
#IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
#FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
#AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
#LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
#OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
#SOFTWARE.

#=========================================================================

# This work has been supported by the French Agence Nationale de la Recherche 
# through the France 2030 program under grant agreement No. ANR-22-PETQ-0008 PQ-TLS.

#=========================================================================

# Author: Clémence Chevignard, Pierre-Alain Fouque & André Schrottenloher
# Date: June 2024
# Version: 2

#=========================================================================
"""
This script corresponds to Algorithm 1 in the paper (Section 4). The value r = 22
is a parameter in the code and can be changed (for example to r = 1, i.e., a single
bit of output, which was used in a previous version of this work).

We separated the precomputation
and the computation parts for two reasons: * to emphasize how the algorithm is
implemented; * because the precomputation is time-consuming (typically an hour
for RSA-2048).

Run with:

python3 classical_simulation.py precompute

to precompute all necessary constants for the RSA-2048 instance that we studied
in the paper, and:

python3 classical_simulation.py run

To run Algorithm 2 on random inputs X. In this script we have hard-coded the
probability of failure of the circuit to 2^(-20), it should generally be
changed depending on the probability of success wanted for the whole algorithm.

"""

from math import log, ceil, floor
import random

from sympy import randprime, primerange, mod_inverse
from util import *

import sys
import os.path
import json
import os

#===============================
# CONSTANTS

# RSA-1024 factoring challenge:
#CONST_N = 135066410865995223349603216278805969938881475605667027524485143851526510604859533833940287150571909441798207282164471551373680419703964191743046496589274256239341020864383202110372958725762358509643110564073501508187510676594629205563685529475213500852879416377328533906109750544334999811150056977236890927563

# we used this instance for testing the success probability
#CONST_N = 3107418240490043721350750035888567930037346022842727545720161948823206440518081504556346829671723286782437916272838033415471073108501919548529007337724822783525742386454014691736602477652346609
#DATA_DIR = "data_rsa_debug"  # where the data will be stored
#CONST_SUCCESS_PROB = 6

# RSA-2048 factoring challenge:
CONST_N = 25195908475657893494027183240048398571429282126204032027777137836043662020707595556264018525880784406918290641249515082189298559149176184502808489120072844992687392807287776735971418347270261896375014971824691165077613379859095700097330459748808428401797429100642458691817195118746121515172654632282216869987549182422433637259085141865462043576798423387184774447920739934236584823824281198163815010674810451660377306056201619676256133844143603833904414952634432190114657544454178424020924616515723350778707749817125772467962926386356373289912154831438167899885040445364023527381951378636564391212010397122822120720357

# sets the failure probability to (roughly) 2^(-20). A higher value may be required
# for larger RSA moduli, if more measurements need to be made
CONST_SUCCESS_PROB = 20
DATA_DIR = "data_rsa_2048"

COFACTORS_FOR_QM = os.path.join(DATA_DIR, "cofactors_for_qm.json")
COFACTORS_FOR_QN = os.path.join(DATA_DIR, "cofactors_for_qn.json")
MP_WP_FIRST_BITS = os.path.join(DATA_DIR, "mp_wp_first_bits.json")
CONSTANTS = os.path.join(DATA_DIR, "constants.json")

#================================


def make_data_dir():
    if not os.path.isdir(DATA_DIR):
        os.mkdir(DATA_DIR)


def label(p, i):
    return "(" + str(p) + ", " + str(i) + ")"


def find_rns_base(u, min_bit_size=18):
    """Computes the RNS primes (we don't use 2)."""
    l = list(primerange(2**min_bit_size, 3 * u))
    final_l = []
    final_prod = 1
    for p in l:  # not 2
        final_l.append(p)
        final_prod += log(p, 2)
        if final_prod > u:
            return final_l
    raise Exception("shouldn't stop here")


def compute_and_store():
    """Preforms the precomputation step."""
    make_data_dir()
    N = CONST_N
    n = ceil(log(N, 2))
    r = 22  # amount of bits of output
    # bit-size of N: n bits
    print("N=", N)

    # maximal bit size of A_X (the product of n/2+ 2l integers smaller than N)
    s = 17  # value for the Ekera-Hastad algorithm, for RSA-2048
    # taken from Table 3 in "On post-processing in the quantum algorithm
    # for computing short discrete logarithms" (Ekera, 2019)
    ell = ceil((n / 2) / s)
    print("ell=", ell)
    m = n // 2 + 2 * ell
    print("m=", m)

    # bit-size required for the RNS: ceil(log(N,2))*(m/2 + m^(2/3))
    max_bit_size = ceil(log(N, 2)) * ceil(m / 2 + pow(m, (2 / 3)))

    modN_success_prob_bits = CONST_SUCCESS_PROB
    # additional bits to increase success probability of the reduction mod N step
    # (the computation of the quotient in the RNS reduction step already succeeds
    # with overwhelming probability)
    # The algorithm should then succeed with probability around 1 - 2^(-20)

    min_bit_size = 18  # of primes in the RNS
    rns_base = find_rns_base(max_bit_size, min_bit_size=min_bit_size)
    print("Computed RNS base")

    #======================================
    M = 1  # rns product
    for p in rns_base:
        M *= p

    # maximal bit size of primes in the RNS (= w in paper)
    max_rns_bit_size = ceil(log(max(rns_base), 2))
    # bit size of the RNS product
    M_bit_size = ceil(log(M, 2))
    # number of primes in RNS
    rns_len = len(rns_base)

    print("Max represented numbers bit-size:", max_bit_size)
    print("Bit-size of RNS product:", M_bit_size)
    print("Max bit size of primes in RNS:", max_rns_bit_size)
    print("Number of primes in RNS:", rns_len)
    _d = {}
    for p in rns_base:
        _bit_size = ceil(log(p, 2))
        if _bit_size not in _d:
            _d[_bit_size] = 0
        _d[_bit_size] += 1
    print("Distribution of bit-size:", _d)

    # setting the size of q_M
    alpha = ceil(log(sum(rns_base), 2))
    q_M_bit_length = max_rns_bit_size + alpha

    # choosing parameter u
    _tmp = 0
    for p in rns_base:
        _tmp += p**2
    u = ceil(log(_tmp, 2)) + 1

    # setting parameter u'
    bits_in_sum = 0
    for p in rns_base:
        bits_in_sum += ceil(log(p, 2))
    u_N = ceil(log(bits_in_sum + q_M_bit_length, 2)) + modN_success_prob_bits

    # constants to store, related to the factoring instance
    constants = dict()
    constants["N_first_bits"] = (N % (2**r))
    constants["N"] = str(N)  # as a number, it would be too big for JSON
    constants["n"] = n
    constants["m"] = m
    constants["ell"] = ell
    constants["modN_success_prob_bits"] = modN_success_prob_bits
    constants["rns_base"] = rns_base
    constants["q_M_bit_length"] = q_M_bit_length
    constants["u"] = u
    constants["u_N"] = u_N
    constants["M_first_bits"] = (M % (2**r))

    with open(CONSTANTS, 'w') as f:
        json.dump(constants, f)

    #=============================
    # computing cofactors for the different steps

    print("Computing cofactors...")

    # setting parameters for Barrett reduction mod N
    barrett_modN_pow2 = n + max_bit_size  # t_N in paper
    barrett_modN_reduction_multiplier = ((2**barrett_modN_pow2) // N)

    M_p_dict = {}
    M_p_inv_dict = {}
    for p in rns_base:
        M_p_dict[p] = M // p
        M_p_inv_dict[p] = mod_inverse(M_p_dict[p], p)

    # computing cofactors for the sum for q_M (C_{p,i} in paper)
    cofactors_for_qM = {}
    for p in rns_base:
        tmp = M_p_inv_dict[p] * (2**u // p)
        for i in range(ceil(log(p, 2))):
            cofactors_for_qM[label(p, i)] = str(
                (2**i * tmp) % (2**(u + q_M_bit_length + 1)))
    print("Computed cofactors for qM")

    # first bits of [M_p w_p] for all p
    Mp_wp_first_bits = {}
    for p in rns_base:
        Mp_wp_first_bits[str(p)] = str(
            ((M_p_dict[p] % (2**r)) * (M_p_inv_dict[p] % (2**r))) % (2**r))

    cofactors_for_QN = {}

    # cofactors for the bits of q which are in the last sum
    tmp = M * barrett_modN_reduction_multiplier
    for i in range(q_M_bit_length):
        cofactors_for_QN[label(0, i)] = str(
            (2**(u_N + r) - ((tmp) >>
                             (barrett_modN_pow2 - u_N - i))) % 2**(u_N + r))

    # this is the step which takes most of the time.
    done = 0
    for p in rns_base:
        done += 1
        if done % 1000 == 0:
            print("Done: ", done, "/", len(rns_base))
        tmp = M_p_dict[p] * M_p_inv_dict[p] * barrett_modN_reduction_multiplier
        for i in range(ceil(log(p, 2))):
            cofactors_for_QN[label(p, i)] = str(
                ((tmp) >> (barrett_modN_pow2 - u_N - i)) % 2**(u_N + r))

    print("Computed cofactors for QN")

    with open(COFACTORS_FOR_QM, 'w') as f:
        json.dump(cofactors_for_qM, f)
    with open(MP_WP_FIRST_BITS, 'w') as f:
        json.dump(Mp_wp_first_bits, f)
    with open(COFACTORS_FOR_QN, 'w') as f:
        json.dump(cofactors_for_QN, f)


def load_and_run():
    """Loads the precomputed data and tests the algorithm."""

    with open(CONSTANTS, 'r') as f:
        constants = json.load(f)
    with open(COFACTORS_FOR_QM, 'r') as f:
        cofactors_for_qM = json.load(f)
    with open(MP_WP_FIRST_BITS, 'r') as f:
        Mp_wp_first_bits = json.load(f)
    with open(COFACTORS_FOR_QN, 'r') as f:
        cofactors_for_QN = json.load(f)

    r = 22
    N = int(constants["N"])
    N_first_bits = N % (2**r)
    m = constants["m"]
    modN_success_prob_bits = constants["modN_success_prob_bits"]
    rns_base = constants["rns_base"]
    q_M_bit_length = constants["q_M_bit_length"]
    u = constants["u"]
    u_N = constants["u_N"]
    M_first_bits = constants["M_first_bits"]
    ell = constants["ell"]
    n = constants["n"]

    print("m", m)
    print("u", u)
    print("u'", u_N)
    print("q_M_bit_length", q_M_bit_length)
    print("Size of result register", r)
    print("Size of qM register", u + q_M_bit_length + 1)
    print("Size of QN register", u_N + r)

    print_every = 10
    repetitions = 100

    # compute numbers for multi-product
    # we use the RSA variant of Ekera-Hastad: select random G invertible modulo N
    G = 2
    # compute H = G^((N-1)/2) - 2^((n/2)-1)
    H = pow(G, (N - 1) // 2 - 2**((n // 2) - 1), N)
    # compute dlog of H over G

    # the numbers to multiply
    A_numbers = ([pow(G, 2**i, N) for i in range(n // 2 + ell)] +
                 [pow(H, -2**i, N) for i in range(ell)])
    A_numbers_mod_rns = {}
    for p in rns_base:
        A_numbers_mod_rns[p] = [(A_numbers[i] % p) for i in range(m)]

    count_fail = 0
    print("Starting!")
    for ctr in range(repetitions):
        if ctr % print_every == 0 and ctr > 0:
            print("Failures:", count_fail / ctr, "tries:", ctr)

        # initial value of control bits
        x_initial = [random.randrange(2) for i in range(m)]

        #=======================
        # for the test: compute the actual value
        A_initial = 1
        for i in range(m):
            if x_initial[i]:
                A_initial *= A_numbers[i]
                A_initial = A_initial % N

        # the value we want
        res_real = A_initial % (2**r)

        #===================
        # here we do the computations in the way the quantum circuit will do them.
        # You can notice that these computations require only small space

        q_M = 0  # same notations as in Algorithm 2 in the paper
        Q_N = 0
        res = 0

        for p in rns_base:
            # compute the RNS residue
            rns_residue = 1
            for i in range(m):
                # controlled modular product
                if x_initial[i]:
                    rns_residue = rns_residue * A_numbers_mod_rns[p][i]
                    rns_residue = (rns_residue % p)

            for i in range(ceil(log(p, 2))):
                # controlled additions
                if ((rns_residue >> i) % 2):
                    q_M += int(cofactors_for_qM[label(p, i)])
                    Q_N += int(cofactors_for_QN[label(p, i)])

            res = (res + rns_residue * int(Mp_wp_first_bits[str(p)])) % (2**r)

        q_M = q_M >> u
        q_M += 1

        res = (res - (q_M * M_first_bits)) % (2**r)
        # at this point, res is zero most of the time

        # finish the computation of Q_N
        for i in range(q_M_bit_length):
            Q_N += (((q_M) >> i) % 2) * int(cofactors_for_QN[label(0, i)])
        # shift the result to get the wanted bits
        Q_N = Q_N >> u_N

        res = (res - (Q_N * N_first_bits)) % (2**r)

        #========================
        #print( res, res_real )
        count_fail += int(res != res_real)

    print(count_fail / repetitions)


if __name__ == "__main__":

    _HELP = """
Usage: 
    python3 classical_simulation.py precompute 
or
    python3 classical_simulation.py run"""
    import sys
    if len(sys.argv) < 2:
        print(_HELP)

    elif sys.argv[1] == "precompute":
        compute_and_store()
    elif sys.argv[1] == "run":
        load_and_run()
    else:
        print(_HELP)
