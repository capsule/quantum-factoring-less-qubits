
/*
Copyright (c) June 2024

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

# This work has been supported by the French Agence Nationale de la Recherche 
# through the France 2030 program under grant agreement No. ANR-22-PETQ-0008 PQ-TLS.

Author: Clémence Chevignard, Pierre-Alain Fouque & André Schrottenloher
Date: June 2024
Version: 2

*/


/*
* This is a re-implementation in C++ of the "run" functionality in  
* "classical_simulation.py", which runs Algorithm 1 from the paper
* with random inputs. The data for the algorithm (N, M, various cofactors)
* needs to have been precomputed using "python3 classical_simulation.py precompute",
* and the directory in which the python script has written its JSON files needs to match
* the directory in which this one will attempt to read them (see below).
*
* The only purpose of this script is that it goes much faster than python, so it
* allows to make more tests and estimate better the probability of success
* of the entire procedure.
*/

#include <chrono>
#include <map>
#include <iostream>
#include <vector>
#include <random>
#include <cmath>
#include <algorithm>

#include <boost/multiprecision/cpp_int.hpp>
#include <boost/integer/mod_inverse.hpp>

#include <string>
#include <fstream>
#include <nlohmann/json.hpp>

using namespace boost::multiprecision;
using namespace boost::integer;
using json = nlohmann::json;
using namespace std;


string dir = "data_rsa_2048/";

string COFACTORS_FOR_QM = dir + "cofactors_for_qm.json";
string COFACTORS_FOR_QN = dir + "cofactors_for_qn.json";
string MP_WP_FIRST_BITS = dir + "mp_wp_first_bits.json";
string CONSTANTS = dir + "constants.json";


inline string label(int p, int i) {
  return "(" + to_string(p) + ", " + to_string(i) + ")";
}


// computes successive products of RNS primes. For an alternative computation
// of residues (which is currently unused)
pair<vector<cpp_int>, map<cpp_int, cpp_int>> product_vector_and_map(const vector<cpp_int>& input_vector) {
  vector<cpp_int> product_vector;
  vector<cpp_int> current_vector = input_vector;
  map<cpp_int, cpp_int> product_map;
  while (current_vector.size() > 1) {
    vector<cpp_int> next_vector;
    for (size_t i = 0; i + 1 < current_vector.size(); i += 2) {
      cpp_int product = (current_vector[i]) * (current_vector[i + 1]);
      next_vector.push_back(product);
      product_map[current_vector[i]] = product;
      product_map[current_vector[i+1]] = product;
      product_vector.push_back(current_vector[i]);
      product_vector.push_back(current_vector[i+1]);
    }
    if (current_vector.size() % 2 == 1) {
      next_vector.push_back( (current_vector.back()) );
    }
      current_vector = next_vector;
  }
  // one element remains
  product_vector.push_back( current_vector.back() );
    
  reverse(product_vector.begin(), product_vector.end());
  return {product_vector, product_map};
}


int main(int argc, char *argv[]) {

  ifstream constants_file(CONSTANTS);
  json constants_data = json::parse(constants_file);

  //------------- load constants

  string N_str = constants_data["N"];
  cpp_int N(N_str);
  int m = constants_data["m"];
  
  vector<int> rns_base = constants_data["rns_base"];
  // max size of RNS primes = 22 bits typically, that's low enough for ints
  
  //--------- For an alternative computation of RNS residues.
  /*
  vector<cpp_int> rns_copy;
  for (auto v: rns_base){
    rns_copy.push_back( cpp_int(v) );
  }
  
  auto pair = product_vector_and_map(rns_copy);
  vector<cpp_int> rns_subproducts = pair.first;
  map<cpp_int, cpp_int> rns_submap = pair.second;
  
  cpp_int A_big_prod;
  map<cpp_int, cpp_int> rns_residues;
  */
  //-----------------------
  
  int r = 22;
  int q_M_bit_length = constants_data["q_M_bit_length"];
  int u = constants_data["u"];
  int u_N = constants_data["u_N"];
  int N_first_bits = constants_data["N_first_bits"]; // r bits, so it fits
  int M_first_bits = constants_data["M_first_bits"]; // r bits, so it fits
  
  int ell = constants_data["ell"];
  int n = constants_data["n"];

  //------------- load tables
  ifstream cofactors_for_qM_file(COFACTORS_FOR_QM);
  // table with str keys (=pairs) and str values
  map<string,string> cofactors_for_qM_json = json::parse(cofactors_for_qM_file);

  ifstream cofactors_for_QN_file(COFACTORS_FOR_QN);
  // table with str keys (=pairs) and str values
  map<string,string> cofactors_for_QN_json = json::parse(cofactors_for_QN_file);

  ifstream Mp_wp_first_bits_file(MP_WP_FIRST_BITS);
  // table with str keys (=RNS primes) and str values
  map<string,string> Mp_wp_first_bits_json = json::parse(Mp_wp_first_bits_file);
  
  // convert to useful data types
  map<int, vector<int256_t>> cofactors_for_qM; // don't need more than 256 bits of precision
  map<int, vector<int256_t>> cofactors_for_QN;
  map<int, int256_t> Mp_wp_first_bits;
  for (int p : rns_base) {
    Mp_wp_first_bits[p] = int256_t(cpp_int(Mp_wp_first_bits_json[to_string(p)]));
    for (int i = 0; i < ceil(log2(p)); ++i) {
      cofactors_for_qM[p].push_back(int256_t(cofactors_for_qM_json[label(p,i)]));
      cofactors_for_QN[p].push_back(int256_t(cofactors_for_QN_json[label(p,i)]));
    }
  }
  for (int i = 0; i < q_M_bit_length; ++i) {
      cofactors_for_QN[0].push_back(int256_t(cofactors_for_QN_json[label(0,i)]));
  }
  //===

  cpp_int G = 2;
  // Calculate H
  cpp_int H = powm(G, (N - 1) / 2 - pow(cpp_int(2), (n / 2) - 1), N);
  cpp_int H_inverse = mod_inverse(H, N);

  // Calculate A_numbers (the A_i)
  vector<cpp_int> A_numbers;
  for (int i = 0; i < n / 2 + ell; ++i) {
    A_numbers.push_back(powm(G, pow(cpp_int(2), i), N));
  }
  for (int i = 0; i < ell; ++i) {
    A_numbers.push_back(powm(H_inverse, pow(cpp_int(2), i), N));
  }
  
  map<int, vector<int64_t>> A_numbers_mod_rns;
  for (int p : rns_base) {
    for (int i = 0; i < m; ++i) {
        A_numbers_mod_rns[p].push_back(int64_t(A_numbers[i] % p));
    }
  }
  //=======================================
  
  int repetitions = 1 << 5;
  int print_every = 10;
  
  // distribution for choosing inputs
  random_device rd;
  mt19937 gen(rd());
  uniform_int_distribution<> dis(0, 1);

  int count_fail = 0;
  
  cpp_int A_initial;
  int256_t res_real, res; // the result and the 'real' one for comparison
  int256_t q_M, Q_N; // we don't need more precision for these numbers
  
  // RNS primes are less than 32 bits, so in the computations & reductions mod p
  // we need less than 64 bits of precision
  int64_t rns_residue;
  
  // do the tests
  auto t = chrono::system_clock::now().time_since_epoch();
  
  
  for (int ctr = 0; ctr < repetitions; ++ctr) {
    if (ctr % print_every == 0 && ctr > 0) {
      cout << "Failures: " << double(count_fail) / double(ctr)
                      << " tries: " << ctr << endl;
    }
    
    // generate random selection of the m numbers
    vector<int> x_initial(m);
    generate(x_initial.begin(), x_initial.end(), [&]() { return dis(gen); });
    
    A_initial = 1;
    for (int i = 0; i < m; ++i) {
      if (x_initial[i]) {
        A_initial *= A_numbers[i];
        A_initial %= N;
      }
    }
    res_real = int256_t( A_initial % (1 << r) );
    
    
    //===========================
    
    q_M = 0; Q_N = 0; res = 0;
    
    //--------- compute all RNS residues by successive reductions
    // (it was not faster)
    /*
    rns_residues[ rns_subproducts[0] ] = A_big_prod % rns_subproducts[0];
    
    for (auto it = rns_subproducts.begin() + 1; it != rns_subproducts.end(); ++it) {
      auto elt = *it;
      // bigger product to which this elt belongs
      cpp_int bigger_product = rns_submap[elt];
      rns_residues[elt] = rns_residues[bigger_product] % elt;
    }*/
    
    // ------------------------

    for (int p : rns_base) {
      
      // compute RNS residue: this dominates the time complexity
      rns_residue = 1;
      for (int i = 0; i < m; ++i) {
        if (x_initial[i]) {
          rns_residue = (rns_residue * A_numbers_mod_rns[p][i]) % p;
        }
      }
      
      // if alternative computation of RNS residues
      //rns_residue = int64_t(rns_residues[p]);
      
      //=====================
      
      int rns_residue_int = int(rns_residue);
      
      for (int i = 0; i < ceil(log2(p)); ++i) {
        if ((rns_residue_int >> i) % 2) {
          q_M += cofactors_for_qM[p][i];
          Q_N += cofactors_for_QN[p][i];
        }
      }
      res = (res + int256_t(rns_residue_int) * Mp_wp_first_bits[p]) % (1 << r);
    }

    q_M >>= u;
    q_M += 1;
    res = (res - (q_M % (1 << r)) * M_first_bits) % (1 << r);
    // In fact, at this point, res is equal to 0 with (very) large probability.

    for (int i = 0; i < q_M_bit_length; ++i) {
      if ((q_M >> i) % 2) {
        Q_N += cofactors_for_QN[0][i];
      }
    }
    Q_N >>= u_N;
    
    res = ((res - (Q_N * N_first_bits)) % (1 << r) + (1<< r) ) % (1 << r);

    cout << res_real << "   " << res << endl;
    if (res_real != res) { count_fail++; }

  }

  cout << "Elapsed time:" << (chrono::system_clock::now().time_since_epoch() - t) / chrono::milliseconds(1) << " ms " << endl;

  return 0;
}


